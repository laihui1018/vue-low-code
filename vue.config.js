/**
 * @type {import('@vue/cli-service').ProjectOptions}
 */
module.exports = {
  publicPath: './',
  outputDir: 'sky-lowcode',
  devServer: {
    port: '2000',
    host: '0.0.0.0',
    open: true
  },
  css: {
    loaderOptions: {
      less: {
        javascriptEnabled: true
      }
    }
  }
}