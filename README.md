# sky-lowcode

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### 预览应用或者生成独立应用时，需要使用 preview.html 模板

preview.html 里面包含页面响应式的代码
