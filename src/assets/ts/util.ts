// 生成唯一id
type UUIDTYPE = 6 | 8 | 12
export const uuid = (length: UUIDTYPE = 8) => {
  return URL.createObjectURL(new Blob()).slice(-length)
}
