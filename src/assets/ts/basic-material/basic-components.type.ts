import { STextClass } from './sText.type'
import { SInputClass } from './sInput.type'
import { SButtonClass } from './sButton.type'
import { SImageClass } from './sImage.type'
import { FlexContainerClass } from './flexContainer.type'

type BasicItemClass = STextClass &
  SInputClass &
  SButtonClass &
  SImageClass &
  FlexContainerClass

/**
 * 基础组件的结构
 */
export interface BasicItem {
  key: string
  type: string
  name: string
  componentName: string
  components?: BasicItem[]
  class: BasicItemClass
}
