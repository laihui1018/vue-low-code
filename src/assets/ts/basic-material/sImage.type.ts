export interface SImageClass {
  url?: string
  isHide?: boolean
  position?: string
  left?: string
  top?: string
  right?: string
  bottom?: string
  zIndex?: string
  width?: string
  height?: string
  display?: string
  margin?: string
  verticalAlign?: string
  opacity?: string
  border?: string
  boxShadow?: string
  borderRadius?: string
}
