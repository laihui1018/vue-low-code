export interface FlexContainerClass {
  isHide?: boolean
  position?: string
  left?: string
  top?: string
  right?: string
  bottom?: string
  zIndex?: string
  width?: string
  height?: string
  flexDirection?: string
  alignItems?: string
  justifyContent?: string
  margin?: string
  padding?: string
  opacity?: string
  backgroundColor?: string
  backgroundImage?: string
  backgroundSize?: string
  backgroundRepeat?: string
  border?: string
  boxShadow?: string
  borderRadius?: string
  boxSizing?: string
}
