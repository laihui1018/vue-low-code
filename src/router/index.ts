import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Layout from '@/views/layout/index.vue'
import AppLayout from '@/views/app-layout/index.vue'
import Login from '@/views/login/index.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/app/:id',
    name: 'AppLayout',
    component: AppLayout,
  },
  {
    path: '/view-app/',
    name: 'ViewApp',
    component: () =>
      import(/* webpackChunkName: "view-app" */ '@/views/view-app/index.vue'),
  },
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    redirect: '/my-app',
    children: [
      {
        path: 'my-app',
        name: 'MyApp',
        component: () =>
          import(/* webpackChunkName: "my-app" */ '@/views/my-app/index.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
})

export default router
