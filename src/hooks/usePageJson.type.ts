import { BasicItem } from '@/assets/ts/basic-material/basic-components.type'

interface AppInfoActions {
  name?: string
}

interface AppInfoModels {
  name?: string
}

/**
 * 页面的配置
 */
export interface PageInfoConfig {
  height: string
  backgroundColor: string
  backgroundImage: string
  backgroundSize: string
  backgroundRepeat: string
}

// 修改页面配置的弹窗
export interface EditPageConfig {
  key: string
  name: string
  height: string
  backgroundColor: string
  backgroundImage: string
  backgroundSize: string
  backgroundRepeat: string
}

/**
 * 应用中的一个页面
 */
export interface PageInfo {
  type: string
  key: string
  name: string
  components?: BasicItem[]
  config: PageInfoConfig
}

/**
 * 一个应用
 */
export interface AppInfo {
  id: string
  name: string
  desc: string
  actions: AppInfoActions
  models: AppInfoModels[]
  pages: PageInfo[]
}
