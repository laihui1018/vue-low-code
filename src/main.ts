import { createApp } from 'vue'
import App from './App.vue'
import { createPinia } from 'pinia'
import router from './router'
import {
  Button,
  Form,
  Input,
  Dropdown,
  Menu,
  Modal,
  Tree,
  Switch,
  Tabs,
} from 'ant-design-vue'
import '@/assets/css/global.less'

const app = createApp(App)
app.use(Button)
app.use(Dropdown)
app.use(Menu)
app.use(Form)
app.use(Input)
app.use(Modal)
app.use(Tree)
app.use(Switch)
app.use(Tabs)
app.use(router).use(createPinia()).mount('#app')
