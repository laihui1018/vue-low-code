// 存放应用的数据
import { defineStore } from 'pinia'
import { MyAppState, AppListItem } from './app.type'

const state: MyAppState = {
  appList: sessionStorage.getItem('appList')
    ? JSON.parse(sessionStorage.getItem('appList') as string)
    : [],
}

export const useAppStore = defineStore('MyApp', {
  state: () => {
    return state
  },
  actions: {
    addAppList(item: AppListItem) {
      this.appList.push(item)
      sessionStorage.setItem('appList', JSON.stringify(this.appList))
    },
    editAppList(item: AppListItem) {
      const idx = this.appList.findIndex((obj) => {
        return item.id === obj.id
      })
      this.appList.splice(idx, 1, item)
      sessionStorage.setItem('appList', JSON.stringify(this.appList))
    },
    deleteAppList(id: string) {
      const idx = this.appList.findIndex((obj) => {
        return id === obj.id
      })
      this.appList.splice(idx, 1)
      sessionStorage.setItem('appList', JSON.stringify(this.appList))
    },
  },
})
