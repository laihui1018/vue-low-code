export interface AppListItem {
  id: string // 应用id
  name: string // 应用名称
  desc: string // 应用描述
}

export interface MyAppState {
  appList: AppListItem[]
}
